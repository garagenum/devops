---
title: "Diariyata Touré"
sitemap:
  priority : 0.1
layout: "search"
link: "https://diariyata.gitlab.io"
tags: 
  - html
  - css
  - php
  - js
status: en poste
position: en recherche d'alternance pour un Bachelor Dev
---

En service civique au Garage Numérique, je m'occupe des sites web des associations partenaires

