---
title: "Florian Roger"
sitemap:
  priority : 0.1
layout: "search"
link: "https://makayabou.gitlab.io/cv"
tags: 
  - python
  - bash
  - django
  - devops
status: en poste
position: contactez-moi pour un partenariat
---

J'anime le projet du Garage Numérique et vous recommande ces candidats formés au #devops #opensource.
