---
title: "Bassiro Mendes"
sitemap:
  priority : 0.1
layout: "search"
link: https://bassirom.gitlab.io/cv/
tags: 
  - python
  - bash
status: formation
position: cherche stage du 22 avril au 22 juin
---


Bonjour, je m'appelle Mendes Bassiro j'ai 19 ans et je suis étudiant au Pass Numérique Pro option Devops au CNAM. Je suis à la recherche d'un stage dans le domaine du Devops afin d'en faire mon métier (développement, administration, sécurité). Ce stage me permettrait d'acquérir encore plus de compétence et de découvrir le travail en entreprise.
