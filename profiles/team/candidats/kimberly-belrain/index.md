---
title: "Kimberly Belrain"
sitemap:
  priority : 0.1
layout: "search"
link: "https://mik99.gitlab.io"
tags: 
  - python
  - bash
  - django
  - HTML / CSS
status: formation
position: cherche stage du 22 avril au 22 juin
---

Avec **Django**, je travaille à la réalisation d'un outil simple pour que des artisans puissent exploiter des demandes de devis via un formulaire web.