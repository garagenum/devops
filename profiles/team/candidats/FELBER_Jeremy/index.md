
---
title: "FELBER Jeremy"
sitemap:
  priority : 0.1
layout: "search"
link: https://reeqz.gitlab.io/cv/
tags: 
  - python
  - bash
  - html
  - CSS
  - hugo
  - nginx
  - virtualbox
status: Auditeur au Conservatoire National des Arts et Métiers.
position: À la recherche d'un stage en tant que technicien hardware à partir du 12 avril au 15 mai.
---


Passionné par la création de contenu, par le monde du numérique et le street workout.
Je suis à la recherche d'un stage afin de pouvoir acquérir des compétences et de renforcer mes connaisances.
