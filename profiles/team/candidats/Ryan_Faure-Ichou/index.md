---
title: "Ryan Faure-ichou"
sitemap:
  priority : 0.1
layout: "search"
link: https://faure-ichou.gitlab.io/cv/
tags: 
  - python
  - bash
  - VM virtuelle
  - ngnix
  - django
status: formation du pass numérique pro au CNAM (conservatoir des arts et métiers)
position: cherche stage du 12 avril au 26 mais
---

Je souhaiterais effectuer un stage de 6 semaine en tant que technicien DEVOPS afin d'acquérir des nouvelles compétences dans votre entreprise
