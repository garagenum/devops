---
title: "Aminata Sama"
sitemap:
  priority : 0.1
layout: "search"
link: https://aminatasama517.gitlab.io/cv/
tags: 
  - python
  - bash
  - virtualbox
  - django
  - nginx
status: formation du pass numérique pro au CNAM (conservatoire des arts et métiers)
position: cherche stage du 19 avril au 22 juin 
--- 

J'ai démarré l'informatique en novembre 2020, et la programmation est en train de devenir une passion.
