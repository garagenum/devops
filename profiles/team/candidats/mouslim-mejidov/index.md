---
title: "Mouslim MEJIDOV"
sitemap:
  priority : 0.1
layout: "search"
link: https://mmouslim97.gitlab.io/cv/
tags: 
  - CSS/HTML
  - django
  - nginx
  - bash
  - python
status: formation
position: stage devops
---

Fasciné depuis toujours par l'informatique, j'ai décidé d'en apprendre davantage et en faire mon métier en me dirigeant vers le DevOps
